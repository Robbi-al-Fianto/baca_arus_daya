<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gedung_m extends CI_Model
{
    public $gedung_table = 'gedung';

    /** 
     * @param string|int $gedung__id
     * @return array|null
     */
    public function getById($gedung__id)
    {
        return $this->db->select()
            ->from($this->gedung_table)
            ->where("{$this->gedung_table}.id", $gedung__id)
            ->limit(1)
            ->get()->row_array();
    }

    public function create($data)
    {
        // SDG = system data gedung

        $prefix_table = 'sdg__';
        $postfix_table = '';

        $this->db->insert($this->gedung_table, $data);

        $last_id = $this->db->insert_id();

        $table_name = "$prefix_table$last_id$postfix_table";
        $createTable = $this->db->query("CREATE TABLE $table_name (
                `id`          int(11) NOT NULL AUTO_INCREMENT,
                `Voltage`     float(11) DEFAULT NULL,
                `Current`     float(11) DEFAULT NULL,
                `Power`       float(11) DEFAULT NULL,
                `Energy`      float(11) DEFAULT NULL,
                `Frequency`   float(11) DEFAULT NULL,
                `PowerFactor` float(11) DEFAULT NULL,
                `created_at` timestamp DEFAULT CURRENT_TIMESTAMP(),
                PRIMARY KEY (`id`)
            )
        ");
        return $createTable;
    }
    public function listGedung($select = '*', $where = [])
    {
        return $this->db->select($select)
            ->from('gedung')
            ->where($where)
            ->get()->result_array();
    }
    public function getFB_gedung()
    {
        $firebase   = $this->firebase->init();
        $db         = $firebase->getDatabase();
        $systemData = $db->getReference('System Data');
        $value      = $systemData->getValue();
        return $value;
    }
    public function destroy($gedung__id)
    {
        // SDG = system data gedung

        $prefix_table = 'sdg__';
        $postfix_table = '';

        $table_name = "$prefix_table$gedung__id$postfix_table";

        $this->db->delete('gedung', ['id' => $gedung__id]); /* Menghapus data yang ada di tabel gedung */

        $createTable = $this->db->query("DROP TABLE $table_name;"); /* Menghapus tabel sdg */

        return $createTable;
    }
}
