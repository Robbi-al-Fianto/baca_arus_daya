<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Usersetting_model extends CI_Model
{
    public function getUser()
    {
        $query = "SELECT `user`. *, `user_role`. `role`
                FROM `user` JOIN `user_role`
                ON `user`.`role_id` = `user_role`.`id`
                WHERE `user`.`is_active` = '1'
                ";

        return $this->db->query($query)->result_array();
    }
}
