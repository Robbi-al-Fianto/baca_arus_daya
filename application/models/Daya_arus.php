<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Daya_arus extends CI_Model
{

    function get_daya()
    {
        $query = $this->db->get('daya_listrik');

        return $query;
    }

    function insert_daya($id_gedung, $voltage, $frekuensi, $arus)
    {
        $data = array(
            'id_gedung' => $id_gedung,
            'voltage' => $voltage,
            'frekuensi' => $frekuensi,
            'arus' => $arus
        );
        $this->db->insert('daya_listrik', $data);
    }

    function update_daya($id, $id_gedung, $voltage, $frekuensi, $arus)
    {
        $this->db->set('id_gedung', $id_gedung);
        $this->db->set('voltage', $voltage);
        $this->db->set('frekuensi', $frekuensi);
        $this->db->set('arus', $arus);
        $this->db->where('id', $id);
        $this->db->update('daya_listrik');
    }

    function delete_daya($id)
    {
        $this->db->update('daya_listrik', [
            'is_active' => '0',
        ], ['id' => $id]);
        // $this->db->delete('daya_listrik', array('id' => $id));
    }
    function getalluser()
    {
        $query = $this->db->query('SELECT * FROM user');
        return $query->num_rows();
    }
    function get_all_daya()
    {
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $query = $this->db->get('daya_listrik');
        return $query->result_array();
    }
}
