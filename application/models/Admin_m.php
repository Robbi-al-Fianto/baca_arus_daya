<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_m extends CI_Model
{

    function get_gedung()
    {
        $query = $this->db->get_where('gedung', ['is_active' => '1'])->result_array();

        return $query;
    }
    function getsdggedung($id)
    {
        $tableName = "sdg__$id";
        $this->db->order_by('created_at', 'DESC');
        // $this->db->limit(1);
        $query = $this->db->get($tableName);
        // return $query->result_array();
        return $query->row_array();
    }

    public function cardRender($gedung)
    {
        $sdg = $gedung['sdg'];
        // $html = '
        //     <div class="card mb-4 mr-4" >
        //         <div class="card-body">
        //             <h5 class="card-title">' . $gedung['nama_gedung'] . '</h5>
        //             <p class="card-text">' . $gedung['address'] . '</p>
        //             <p class="card-text">' . $gedung['etc'] . '</p>
        //         </div>
        //         <ul class="list-group list-group-flush">
        //             <li class="list-group-item">Voltage     : ' . (isset($sdg['Voltage'])     ? $sdg['Voltage']     : ' ') . '</li>
        //             <li class="list-group-item">Current     : ' . (isset($sdg['Current'])     ? $sdg['Current']     : ' ') . '</li>
        //             <li class="list-group-item">Power       : ' . (isset($sdg['Power'])       ? $sdg['Power']       : ' ') . '</li>
        //             <li class="list-group-item">Energy      : ' . (isset($sdg['Energy'])      ? $sdg['Energy']      : ' ') . '</li>
        //             <li class="list-group-item">Frequency   : ' . (isset($sdg['Frequency'])   ? $sdg['Frequency']   : ' ') . '</li>
        //             <li class="list-group-item">PowerFactor : ' . (isset($sdg['PowerFactor']) ? $sdg['PowerFactor'] : ' ') . '</li>
        //             <li class="list-group-item">Updated_at  : ' . (isset($sdg['created_at'])  ? $sdg['created_at']  : ' ') . '</li>
        //         </ul> 
        //     </div>';
        // return $html;
    }
}
