<!-- Begin Page Content -->
<div class="container-fluid" id="realtime">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div id="cardPage" class="row">

    </div>
    <!-- <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item text-gray-800" src="http://192.168.16.208:3000/d-solo/PuCG1nf7k/tes-dashboard?orgId=1&refresh=5s" allowfullscreen></iframe>
    </div> -->
    <br>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script>
    function ajaxRequest() {
        $.ajax({
            url: "<?= site_url('Member/sendmail'); ?>",
            type: 'GET',
            success: function(data) {
                console.log(data)
                $('#cardPage').html(data);
            },
            error: function(jqXHR, textStatus) {
                alert(textStatus);
            },
        })
    };
    ajaxRequest();
    setInterval(function() {
        ajaxRequest();
    }, 5000);
</script>