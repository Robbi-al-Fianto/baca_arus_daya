<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="row">
        <div class="col-lg-6">
            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <?= $this->session->flashdata('message'); ?>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Image</th>
                        <th scope="col">Date Created</th>
                        <th scope="col">Role</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($User as $keyNum => $u) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $u['name']; ?></td>
                            <td><?= $u['email']; ?></td>
                            <td><img src="<?= base_url('assets/img/profile/') . $u['image'] ?>" class="img-fluid rounded-start"></td>
                            <td> <?= date('d F Y (l)', $u['date_created']); ?></td>
                            <td><?= $u['role']; ?></td>
                            <td>
                                <?php if ($u['id'] != $this->session->userdata('user_id')) : /* Jika id user TIDAK SAMA DENGAN id user yang sedang login, maka tampilkan berikut. */ ?>
                                    <button onclick="update_user('<?php echo $keyNum; ?>')" class="badge badge-success">edit</button>
                                    <button onclick="delete_user('<?php echo $keyNum; ?>')" class=" badge badge-danger">delete</button>
                                <?php endif; ?>
                            </td>

                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>


        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Update Modal -->
<div class="modal fade" id="updateUserModal" tabindex="-1" role="dialog" aria-labelledby="updateUserModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateUserModalLabel">Change User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('usersetting/updateuser'); ?>" method="post">
                <input type="hidden" name="edit_id">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="edit_name" name="edit_name" placeholder="Name" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="edit_email" name="edit_email" placeholder="Email" readonly>
                    </div>
                    <div class="form-group">
                        <select name="edit_role_id" id="edit_role_id" class="form-control" required>
                            <option value=""> Select Role</option>
                            <?php foreach ($role as $r) : ?>
                                <option value="<?= $r['id']; ?>"><?= $r['role']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Delete Product -->
<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="deleteUserModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteUserModalLabel">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger">
                    Are You Sure To Delete This User?
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <form action="<?php echo base_url('usersetting/deleteuser'); ?>" method="post">
                    <input type="hidden" id="delete_id" name="id" value="<?php echo trim($u['id']); ?>">
                    <button type="submit" class="btn btn-primary btn-delete">Yes</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    let User = JSON.parse(`<?php echo json_encode($User) ?>`);

    function update_user(keyNum) {
        let dataUser = User[keyNum];

        $('[name="edit_id"]').val(dataUser['id']);
        $('#edit_name').val(dataUser['name']);
        $('#edit_email').val(dataUser['email']);
        $('#edit_role_id').val(dataUser['role_id']).trigger('change');

        $('#updateUserModal').modal('show');
    }

    function delete_user(keyNum) {
        let dataUser = User[keyNum];
        let id = dataUser['id'];
        $('#delete_id').val(id);

        $('#deleteUserModal').modal('show');
    }
</script>