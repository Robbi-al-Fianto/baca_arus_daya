<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <!-- /.pusher-start -->


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Real Time CRUD Codeigniter</h2>

                <?= $this->session->flashdata('message'); ?>

                <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#ModalAdd">Add New Daya</button>
                <table id="mytable" class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Gedung</th>
                            <th scope="col">Voltage</th>
                            <th scope="col">Frekuensi</th>
                            <th scope="col">Arus</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="show_daya">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal Add New Daya -->
    <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Daya</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="input1">Nama Gedung</label>
                        <input type="text" name="id_gedung" class="form-control id_gedung" id="input1" placeholder="Nama Gedung">
                    </div>
                    <div class="form-group">
                        <label for="input2">Voltage</label>
                        <input type="text" name="voltage" class="form-control voltage" id="input2" placeholder="Voltage">
                    </div>
                    <div class="form-group">
                        <label for="input3">Frekuensi</label>
                        <input type="text" name="frekuensi" class="form-control frekuensi" id="input3" placeholder="Frekuensi">
                    </div>
                    <div class="form-group">
                        <label for="input4">Arus</label>
                        <input type="text" name="arus" class="form-control arus" id="input4" placeholder="Arus">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal Edit Product -->
    <div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Daya</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="input1">Nama Gedung</label>
                        <input type="text" name="id_gedung" class="form-control id_gedung_edit" id="input1" placeholder="Nama Gedung">
                    </div>
                    <div class="form-group">
                        <label for="input2">Voltage</label>
                        <input type="text" name="voltage" class="form-control voltage_edit" id="input2" placeholder="Voltage">
                    </div>
                    <div class="form-group">
                        <label for="input3">Frekuensi</label>
                        <input type="text" name="frekuensi" class="form-control frekuensi_edit" id="input3" placeholder="Frekuensi">
                    </div>
                    <div class="form-group">
                        <label for="input4">Arus</label>
                        <input type="text" name="arus" class="form-control arus_edit" id="input4" placeholder="Arus">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" class="id_edit">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-edit">Edit</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Delete Product -->
    <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info">
                        Are you sure to delete this data?
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" class="id">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-primary btn-delete">Yes</button>
                </div>
            </div>
        </div>
    </div>



    <!-- /.pusher-end -->



</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->