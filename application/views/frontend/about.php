<div class="intro-section" style="background-image: url('<?= base_url('assetsfe/')  ?>images/port.jpg');">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-7 mx-auto text-center" data-aos="fade-up">
        <h1>Klaster Bisnis Utama Pelindo</h1>
        <p>Ada empat klaster bisnis utama Pelindo, yaitu:

        <p>1. Klaster petikemas</p>
        <p>2. Klaster non petikemas</p>
        <p>3. Klaster logistik & pengembangan daerah pesisir (hinterland)</p>
        <p>4. Klaster kelautan, peralatan, dan pelayanan pelabuhan</p>
        </p>
        <p><a href="<?= site_url('frontend/contact'); ?>" class="btn btn-primary py-3 px-5">Contact</a></p>
      </div>
    </div>
  </div>
</div>


<div class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <img src="<?= base_url('assetsfe/')  ?>images/port2.jpg" alt="Image" class="img-fluid">
      </div>
      <div class="col-md-6">
        <span class="text-serif text-primary">About Us</span>
        <h3 class="heading-92913 text-black">Connecting the globe with the world's largest archipelago</h3>
        <p>Berbasis di negara kepulauan terbesar dengan sejarah panjang pengaruh maritimnya di dunia, Pelabuhan Indonesia (Pelindo) adalah pelabuhan kelas dunia yang menawarkan anda layanan terintegrasi antar pelabuhan di Indonesia. Pelindo merupakan perusahaan hasil integrasi dari empat (4) BUMN pelabuhan yaitu PT Pelindo I (Persero), PT Pelindo II (Persero), PT Pelindo III (Persero) dan PT Pelindo IV (Persero) yang resmi berdiri pada tanggal 1 Oktober 2021.</p>
        <p><a href="#" class="btn btn-primary py-3 px-4">Learn More</a></p>
      </div>
    </div>
  </div>
</div>

<div class="site-section">
  <div class="container">
    <div class="row justify-content-center mb-5">
      <div class="col-md-7 text-center">
        <span class="text-serif text-primary">Team</span>
        <h3 class="heading-92913 text-black text-center">Our Team</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-6 mb-lg-0">
        <div class="person">
          <figure>
            <img src="<?= base_url('assetsfe/')  ?>images/ridho2.jpeg" alt="Image" class="img-fluid" style="width:400px;height:320px;">
            <div class="social">
              <a href="#"><span class="icon-facebook"></span></a>
              <a href="#"><span class="icon-twitter"></span></a>
              <a href="#"><span class="icon-linkedin"></span></a>
            </div>
          </figure>
          <div class="person-contents">
            <h3>Ridho Fathoni P</h3>
            <span class="position">D4 Computer Engineering</span>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 mb-lg-0">
        <div class="person">
          <figure>
            <img src="<?= base_url('assetsfe/')  ?>images/fatih.jpeg" alt="Image" class="img-fluid" style="width:400px;height:320px;">
            <div class="social">
              <a href="#"><span class="icon-facebook"></span></a>
              <a href="#"><span class="icon-twitter"></span></a>
              <a href="#"><span class="icon-linkedin"></span></a>
            </div>
          </figure>
          <div class="person-contents">
            <h3>M. Fatih Aisy</h3>
            <span class="position">D4 Computer Engineering</span>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 mb-lg-0">
        <div class="person">
          <figure>
            <img src="<?= base_url('assetsfe/')  ?>images/rafli.jpeg" alt="Image" class="img-fluid" style="width:400px;height:320px;">
            <div class="social">
              <a href="#"><span class="icon-facebook"></span></a>
              <a href="#"><span class="icon-twitter"></span></a>
              <a href="#"><span class="icon-linkedin"></span></a>
            </div>
          </figure>
          <div class="person-contents">
            <h3>M. Rafly Trisnayudha</h3>
            <span class="position">D4 Computer Engineering</span>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6 mb-lg-0">
        <div class="person">
          <figure>
            <img src="<?= base_url('assetsfe/')  ?>images/robbi.jpg" alt="Image" class="img-fluid" style="width:400px;height:320px;">
            <div class="social">
              <a href="#"><span class="icon-facebook"></span></a>
              <a href="#"><span class="icon-twitter"></span></a>
              <a href="#"><span class="icon-linkedin"></span></a>
            </div>
          </figure>
          <div class="person-contents">
            <h3>Robbi Al Fianto</h3>
            <span class="position">D3 Information Systems</span>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="site-section bg-image overlay" style="background-image: url('<?= base_url('assetsfe/')  ?>images/port2.jpg');">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-7 text-center">
        <h2 class="text-white">Get In Touch With Us</h2>
        <p class="lead text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        <p class="mb-0"><a href="<?= site_url('frontend/contact'); ?>" class="btn btn-warning py-3 px-5 text-white">Contact Us</a></p>
      </div>
    </div>
  </div>
</div>