<div class="intro-section site-blocks-cover innerpage" style="background-image: url('<?= base_url('assetsfe/')  ?>images/port.jpg');">
    <div class="container">
        <div class="row align-items-center text-center border">
            <div class="col-lg-12 mt-5" data-aos="fade-up">
                <h1>Get In Touch</h1>
                <p class="text-white text-center">
                    <a href="index.html">Home</a>
                    <span class="mx-2">/</span>
                    <span>Contact Us</span>
                </p>
            </div>
        </div>
    </div>
</div>


<div class="site-section">
    <div class="container">
        <?= $this->session->flashdata('message'); ?>
        <form action="<?= site_url('frontend/send_email'); ?>" method="post">

            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="fname">First Name</label>
                    <input type="text" name="fname" id="fname" class="form-control form-control-lg" required>
                </div>
                <div class="col-md-6 form-group">
                    <label for="lname">Last Name</label>
                    <input type="text" name="lname" id="lname" class="form-control form-control-lg" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="eaddress">Email Address</label>
                    <input type="text" name="eaddress" id="eaddress" class="form-control form-control-lg" required>
                </div>
                <div class="col-md-6 form-group">
                    <label for="subject">Subject</label>
                    <input type="text" name="subject" id="subject" class="form-control form-control-lg" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    <label for="message">Message</label>
                    <textarea name="message" id="message" cols="30" rows="10" class="form-control" required></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <input type="submit" value="Send Message" class="btn btn-primary rounded-0 px-3 px-5">
                </div>
            </div>

        </form>
    </div>
</div>

<div class="section-bg style-1" style="background-image: url('<?= base_url('assetsfe/')  ?>images/port.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                <span class="icon flaticon-mortarboard"></span>
                <h3>Our Philosphy</h3>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis recusandae, iure repellat quis delectus ea? Dolore, amet reprehenderit.</p>
            </div>
            <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                <span class="icon flaticon-school-material"></span>
                <h3>Academics Principle</h3>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis recusandae, iure repellat quis delectus ea?
                    Dolore, amet reprehenderit.</p>
            </div>
            <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                <span class="icon flaticon-library"></span>
                <h3>Key of Success</h3>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis recusandae, iure repellat quis delectus ea?
                    Dolore, amet reprehenderit.</p>
            </div>
        </div>
    </div>
</div>