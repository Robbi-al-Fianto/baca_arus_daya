<div class="hero-slide owl-carousel site-blocks-cover">
  <div class="intro-section" style="background-image: url('<?= base_url('assetsfe/')  ?>images/port.jpg');">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-7 ml-auto text-justify" data-aos="fade-up">
          <h1>VISI</h1>
          <p>Menjadi pemimpin ekosistem maritim terintegrasi dan berkelas dunia. Visi tersebut merupakan pernyataan cita-cita Perusahaan menjadi pintu gerbang utama jaringan logistik global di Indonesia. Cita-cita ini muncul dilandasi dengan potensi geografis, peluang bisnis serta kebijakan nasional yang membuka peluang bagi perusahaan untuk merealisasikan visi dimaksud.</p>
          <p><a href="#" class="btn btn-primary py-3 px-5">Read More</a></p>
        </div>
      </div>
    </div>
  </div>

  <div class="intro-section" style="background-image: url('<?= base_url('assetsfe/')  ?>images/port2.jpg');">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-7 mx-auto text-center" data-aos="fade-up">
          <h1>MISI</h1>
          <p>Mewujudkan jaringan ekosistem maritim nasional melalui peningkatan konektivitas jaringan dan integrasi pelayanan guna mendukung pertumbuhan ekonomi negara</p>
          <p><a href="#" class="btn btn-primary py-3 px-5">Read More</a></p>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- END slider -->

<div class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <img src="<?= base_url('assetsfe/')  ?>images/port2.jpg" alt="Image" class="img-fluid">
      </div>
      <div class="col-md-6">
        <span class="text-serif text-primary">About Us</span>
        <h3 class="heading-92913 text-black">Connecting the globe with the world's largest archipelago</h3>
        <p>Berbasis di negara kepulauan terbesar dengan sejarah panjang pengaruh maritimnya di dunia, Pelabuhan Indonesia (Pelindo) adalah pelabuhan kelas dunia yang menawarkan anda layanan terintegrasi antar pelabuhan di Indonesia. Pelindo merupakan perusahaan hasil integrasi dari empat (4) BUMN pelabuhan yaitu PT Pelindo I (Persero), PT Pelindo II (Persero), PT Pelindo III (Persero) dan PT Pelindo IV (Persero) yang resmi berdiri pada tanggal 1 Oktober 2021.</p>
        <p><a href="#" class="btn btn-primary py-3 px-4">Learn More</a></p>
      </div>
    </div>
  </div>
</div>

<div class="py-5">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-4 col-lg-2">
        <div class="service-29283 justify-content-between">
          <span class="wrap-icon-39293">
            <span class="flaticon-yacht"></span>
          </span>
          <h3>Amanah</h3>
          <p> Integritas, Terpercaya, Bertanggung Jawab, Komitmen, Akuntabilitas, Jujur, Disiplin</p>
        </div>
      </div>
      <div class="col-md-4 col-lg-2">
        <div class="service-29283">
          <span class="wrap-icon-39293">
            <span class="flaticon-shield"></span>
          </span>
          <h3>Kompeten</h3>
          <p>Profesional, Fokus Pelanggan, Pelayanan Memuaskan, Unggul, Excellence, Smart</p>
        </div>
      </div>
      <div class="col-md-4 col-lg-2">
        <div class="service-29283">
          <span class="wrap-icon-39293">
            <span class="flaticon-captain"></span>
          </span>
          <h3>Harmonis</h3>
          <p>Peduli (Caring), Keberagaman (Diversity)</p>
        </div>
      </div>
      <div class="col-md-4 col-lg-2">
        <div class="service-29283">
          <span class="wrap-icon-39293">
            <span class="flaticon-badge"></span>
          </span>
          <h3>Loyal</h3>
          <p>Komitmen, Dedikasi (rela berkorban), Kontribusi</p>
        </div>
      </div>
      <div class="col-md-4 col-lg-2">
        <div class="service-29283">
          <span class="wrap-icon-39293">
            <span class="flaticon-captain-1"></span>
          </span>
          <h3>Adaptif</h3>
          <p>Inovatif, Agile, Adaptif</p>
        </div>
      </div>
      <div class="col-md-4 col-lg-2">
        <div class="service-29283">
          <span class="wrap-icon-39293">
            <span class="flaticon-devices"></span>
          </span>
          <h3>Kolaboratif</h3>
          <p>Kerja Sama, Sinergi</p>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="site-section">
  <div class="container">
    <div class="row justify-content-center mb-5">
      <div class="col-md-7 text-center">
        <span class="text-serif text-primary">Testimonial</span>
        <h3 class="heading-92913 text-black text-center">What Customer Saying...</h3>
      </div>
    </div>
    <div class="row">
      <div class="mb-4 mb-lg-0 col-md-6 col-lg-4">
        <div class="testimony-39291">
          <blockquote>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem laborum cumque commodi, unde doloribus.</p>
          </blockquote>
          <div class="d-flex vcard align-items-center">
            <div class="pic mr-3">
              <img src="<?= base_url('assetsfe/')  ?>images/person_3_sq.jpg" alt="Image" class="img-fluid">
            </div>
            <div class="text">
              <strong class="d-block">John Doe</strong>
              <span>CEO and Co-Founder</span>
            </div>
          </div>
        </div>
      </div>
      <div class="mb-4 mb-lg-0 col-md-6 col-lg-4">
        <div class="testimony-39291">
          <blockquote>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem laborum cumque commodi, unde doloribus.</p>
          </blockquote>
          <div class="d-flex vcard align-items-center">
            <div class="pic mr-3">
              <img src="<?= base_url('assetsfe/')  ?>images/person_4_sq.jpg" alt="Image" class="img-fluid">
            </div>
            <div class="text">
              <strong class="d-block">John Doe</strong>
              <span>CEO and Co-Founder</span>
            </div>
          </div>
        </div>
      </div>
      <div class="mb-4 mb-lg-0 col-md-6 col-lg-4">
        <div class="testimony-39291">
          <blockquote>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem laborum cumque commodi, unde doloribus.</p>
          </blockquote>
          <div class="d-flex vcard align-items-center">
            <div class="pic mr-3">
              <img src="<?= base_url('assetsfe/')  ?>images/person_3_sq.jpg" alt="Image" class="img-fluid">
            </div>
            <div class="text">
              <strong class="d-block">John Doe</strong>
              <span>CEO and Co-Founder</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="site-section bg-image overlay" style="background-image: url('<?= base_url('assetsfe/')  ?>images/port2.jpg');">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-7 text-center">
        <h2 class="text-white">Get In Touch With Us</h2>
        <p class="lead text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        <p class="mb-0"><a href="<?= site_url('frontend/contact'); ?>" class="btn btn-warning py-3 px-5 text-white">Contact Us</a></p>
      </div>
    </div>
  </div>
</div>