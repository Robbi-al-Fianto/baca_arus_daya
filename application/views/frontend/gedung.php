<div class="section-bg style-1" style="background-image: url('<?= base_url('assetsfe/')  ?>images/port.jpg');">
    <div class="container pt-4">
        <div class="row pt-5">
            <?php if (count($list_gedung)) : ?>
                <?php foreach ($list_gedung as $gdng) : ?>
                    <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                        <div class="card w-100 m-3">
                            <?php
                            $relativePath = 'assets/img/gedung/' . $gdng['foto'];
                            $fullPath     = FCPATH . $relativePath;
                            $isFileExist  = file_exists($fullPath) && is_file($fullPath);
                            $url = 'http://via.placeholder.com/640x360';
                            if ($isFileExist) {
                                $url = base_url($relativePath);
                            };
                            ?>
                            <img class="card-img-top" src="<?= $url; ?>" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title"><?= $gdng['nama_gedung']; ?></h5>
                                <h6 class="card-subtitle mb-2 text-muted"><?= $gdng['address']; ?></h6>
                                <p class="card-text"><?= $gdng['etc']; ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else : ?>
            <?php endif; ?>
        </div>
    </div>
</div>