 <!-- Footer -->
 <footer class="sticky-footer bg-white">
     <div class="container my-auto">
         <div class="copyright text-center my-auto">
             <span>Copyright &copy; Monitoring Arus <?= date('Y') ?> </span>
         </div>
     </div>
 </footer>
 <!-- End of Footer -->

 </div>
 <!-- End of Content Wrapper -->

 </div>
 <!-- End of Page Wrapper -->

 <!-- Scroll to Top Button-->
 <a class="scroll-to-top rounded" href="#page-top">
     <i class="fas fa-angle-up"></i>
 </a>

 <!-- Logout Modal-->
 <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                 <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                 </button>
             </div>
             <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
             <div class="modal-footer">
                 <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                 <a class="btn btn-primary" href="<?= base_url('auth/logout') ?>">Logout</a>
             </div>
         </div>
     </div>
 </div>

 <!-- Bootstrap core JavaScript-->
 <script src="<?= base_url('assets/')  ?>vendor/jquery/jquery.min.js"></script>
 <script src="<?= base_url('assets/')  ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

 <!-- Core plugin JavaScript-->
 <script src="<?= base_url('assets/')  ?>vendor/jquery-easing/jquery.easing.min.js"></script>

 <!-- Custom scripts for all pages-->
 <script src="<?= base_url('assets/')  ?>js/sb-admin-2.min.js"></script>


 <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.js"></script>


 <script>
     $('.form-check-input').on('click', function() {

         const menuId = $(this).data('menu');
         const roleId = $(this).data('role');

         $.ajax({
             url: "<?= base_url('admin/changeaccess'); ?>",
             type: 'post',
             data: {
                 menuId: menuId,
                 roleId: roleId
             },
             success: function() {
                 document.location.href = "<?= base_url('admin/roleaccess/'); ?>" + roleId;
             }
         });

     });
 </script>



 <script>
     $(document).ready(function() {
         // CALL FUNCTION SHOW Daya
         show_daya();

         // Enable pusher logging - don't include this in production
         Pusher.logToConsole = true;

         var pusher = new Pusher('f02b5d8a1b4f1d7524de', {
             cluster: 'ap1',
             forceTLS: true
         });

         var channel = pusher.subscribe('my-channel');
         channel.bind('my-event', function(data) {
             if (data.message === 'success') {
                 show_daya();
             }
         });

         // FUNCTION SHOW Daya
         function show_daya() {
             $.ajax({
                 url: '<?php echo site_url("building/get_daya"); ?>',
                 type: 'GET',
                 async: true,
                 dataType: 'json',
                 success: function(data) {
                     var html = '';
                     var count = 1;
                     var i;
                     for (i = 0; i < data.length; i++) {
                         html += '<tr>' +
                             '<th>' + count++ + '</th>' +
                             '<td>' + data[i].id_gedung + '</td>' +
                             '<td>' + data[i].voltage + '</td>' +
                             '<td>' + data[i].frekuensi + '</td>' +
                             '<td>' + data[i].arus + '</td>' +
                             '<td>' +
                             '<a href="javascript:void(0);" class="badge badge-info item_edit" data-id="' + data[i].id + '" data-id_gedung="' + data[i].id_gedung + '"data-voltage="' + data[i].voltage + '" data-frekuensi="' + data[i].frekuensi + '" data-arus="' + data[i].arus + '">edit</a>' +
                             '<a href="javascript:void(0);" class="badge badge-danger item_delete" data-id="' + data[i].id + '">delete</a>' +
                             '</td>' +
                             '</tr>';
                     }
                     $('.show_daya').html(html);
                 }

             });
         }

         // CREATE NEW DAYA
         $('.btn-save').on('click', function() {
             var id_gedung = $('.id_gedung').val();
             var voltage = $('.voltage').val();
             var frekuensi = $('.frekuensi').val();
             var arus = $('.arus').val();
             $.ajax({
                 url: '<?php echo site_url("building/create"); ?>',
                 method: 'POST',
                 data: {
                     id_gedung: id_gedung,
                     voltage: voltage,
                     frekuensi: frekuensi,
                     arus: arus,
                 },
                 success: function() {
                     $('#ModalAdd').modal('hide');
                     $('.id_gedung').val("");
                     $('.voltage').val("");
                     $('.frekuensi').val("");
                     $('.arus').val("");
                 }
             });
         });
         // END CREATE PRODUCT

         // UPDATE PRODUCT
         $('#mytable').on('click', '.item_edit', function() {
             var id = $(this).data('id');
             var id_gedung = $(this).data('id_gedung');
             var voltage = $(this).data('voltage');
             var frekuensi = $(this).data('frekuensi');
             var arus = $(this).data('arus');
             $('#ModalEdit').modal('show');
             $('.id_edit').val(id);
             $('.id_gedung_edit').val(id_gedung);
             $('.voltage_edit').val(voltage);
             $('.frekuensi_edit').val(frekuensi);
             $('.arus_edit').val(arus);
         });

         $('.btn-edit').on('click', function() {
             var id = $('.id_edit').val();
             var id_gedung = $('.id_gedung_edit').val();
             var voltage = $('.voltage_edit').val();
             var frekuensi = $('.frekuensi_edit').val();
             var arus = $('.arus_edit').val();
             $.ajax({
                 url: '<?php echo site_url("building/update"); ?>',
                 method: 'POST',
                 data: {
                     id: id,
                     id_gedung: id_gedung,
                     voltage: voltage,
                     frekuensi: frekuensi,
                     arus: arus,
                 },
                 success: function() {
                     $('#ModalEdit').modal('hide');
                     $('.id_edit').val("");
                     $('.id_gedung_edit').val("");
                     $('.voltage_edit').val("");
                     $('.frekuensi_edit').val("");
                     $('.arus_edit').val("");
                 }
             });
         });
         // END EDIT PRODUCT

         // DELETE PRODUCT
         $('#mytable').on('click', '.item_delete', function() {
             var id = $(this).data('id');
             $('#ModalDelete').modal('show');
             $('.id').val(id);
         });

         $('.btn-delete').on('click', function() {
             var id = $('.id').val();
             $.ajax({
                 url: '<?php echo site_url("building/delete"); ?>',
                 method: 'POST',
                 data: {
                     id: id
                 },
                 success: function() {
                     $('#ModalDelete').modal('hide');
                     $('.id').val("");
                 }
             });
         });
         // END DELETE PRODUCT

     });
 </script>
 <script>
     //  window.onload = function() {
     //      var getalldaya = document.getElementById("getalldaya");
     //  }

     //  $(document).ready(function() {
     //      setInterval(function() {
     //          $(data).load("admin/index");
     //      }, 5000);
     //  });

     //  function fetchdata() {
     //      $.ajax({
     //     
     //          type: 'post',
     //          //  data: {
     //          //      gedungId: gedungId,
     //          //      voltageId: voltageId,
     //          //      frekuensiId: frekuensiId,
     //          //      arusId: arusId
     //          //  },
     //      });
     //  }

     //  $(document).ready(function() {
     //      setInterval(fetchdata, 1000);
     //  });
 </script>


 </body>

 </html>