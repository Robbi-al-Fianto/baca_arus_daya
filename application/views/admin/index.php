<!-- Begin Page Content -->
<div class="container-fluid" id="realtime">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div id="cardPagemysql" class="row">

    </div>
    <div id="cardPage" class="row">

    </div>
    <div class="embed-responsive embed-responsive-16by9">
        <!-- <iframe src="http://<?= $_SERVER['HTTP_HOST']; ?>:3000/d-solo/PuCG1nf7k/monitoring-arus?orgId=1&refresh=5s&from=1625055905615&to=1656591905615&panelId=2" width="450" height="200" frameborder="0"></iframe> -->
        <iframe src="http://<?= $_SERVER['HTTP_HOST']; ?>:3000/d-solo/PuCG1nf7k/monitoring-arus?orgId=1&from=1640995200000&to=1672531199999&refresh=5s&panelId=2" width="450" height="200" frameborder="0"></iframe>" width="450" height="200" frameborder="0"></iframe>
        <!-- <iframe class="embed-responsive-item text-gray-800" src="http://localhost:3000/goto/stIZzjq7z?orgId=1&inactive=1"></iframe> -->
        <!-- <iframe class="embed-responsive-item text-gray-800" src="https://snapshots.raintank.io/dashboard/snapshot/ec0UhTxsbYIUvmlGE1vnBvbuwtd7SCYN" allowfullscreen></iframe> -->
    </div>
    <br>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script>
    function ajaxRequest() {
        $.ajax({
            url: "<?= site_url('Admin/senddatabase'); ?>",
            type: 'GET',
            success: function(data) {
                console.log(data)
                $('#cardPage').html(data);
            },
            error: function(jqXHR, textStatus) {
                alert(textStatus);
            },
        })
    };

    ajaxRequest();
    setInterval(function() {
        ajaxRequest();
    }, 5000);

    function ajaxRequest1() {
        $.ajax({
            url: "<?= site_url('Admin/sendmysql'); ?>",
            type: 'GET',
            success: function(data) {
                console.log(data)
                $('#cardPagemysql').html(data);
            },
            error: function(jqXHR, textStatus) {
                alert(textStatus);
            },
        })
    };
    ajaxRequest1();
    setInterval(function() {
        ajaxRequest1();
    }, 5000);
</script>