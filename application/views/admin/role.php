<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div class="row">
        <div class="col-lg-6">

            <?= form_error('menu', '<div class="alert alert-danger" role="alert">
                    Role Field is Required</div>') ?>

            <?= $this->session->flashdata('message'); ?>

            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newRoleModal">Add New Role</a>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Role</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($role as $keyNum => $r) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $r['role']; ?></td>
                            <td>
                                <button type="button" onclick="location.href='<?= base_url('admin/roleaccess/') . $r['id']; ?>'" class="badge badge-warning">access</button>
                                <!-- <?php //if ($r['id'] != $this->session->userdata('role_id')) : 
                                        ?> -->
                                <?php if ($r['id'] > 2) : ?>
                                    <button onclick="update_role('<?php echo $keyNum; ?>')" class="badge badge-success">edit</button>
                                    <button onclick="delete_role('<?php echo $keyNum; ?>')" class=" badge badge-danger">delete</button>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>


        </div>
    </div>



</div>
<!-- /.container-fluid -->
</div>


<!-- End of Main Content -->

<!-- Modal -->

<!-- Modal -->
<div class="modal fade" id="newRoleModal" tabindex="-1" role="dialog" aria-labelledby="newRoleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newRoleModalLabel">Add New Role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/role'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="add_nama_gedung">Input Role<sup class="text-danger">*</sup></label>
                        <input type="text" class="form-control" id="role" name="role" placeholder="Role name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Update Modal -->
<div class="modal fade" id="updateRoleModal" tabindex="-1" role="dialog" aria-labelledby="updateRoleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateRoleModalLabel">Edit Role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('admin/updaterole'); ?>" method="post">
                <input type="hidden" name="edit_id">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="edit_role" name="edit_role" placeholder="Menu name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Delete Product -->
<div class="modal fade" id="deleteRoleModal" tabindex="-1" role="dialog" aria-labelledby="deleteRoleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteRoleModalLabel">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger">
                    Are You Sure To Delete This Role?
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <form action="<?php echo base_url('admin/deleterole'); ?>" method="post">
                    <input type="hidden" id="delete_id" name="id" value="<?php echo trim($r['id']); ?>">
                    <button type="submit" class="btn btn-primary btn-delete">Yes</button>
                </form>
            </div>
        </div>
    </div>
</div>



<script>
    let role = JSON.parse(`<?php echo json_encode($role) ?>`);

    function update_role(keyNum) {
        let dataRole = role[keyNum];
        $('[name="edit_id"]').val(dataRole['id']);
        $('#edit_role').val(dataRole['role']);

        $('#updateRoleModal').modal('show');
    }

    function delete_role(keyNum) {
        let dataRole = role[keyNum];
        let id = dataRole['id'];
        $('#delete_id').val(id);

        $('#deleteRoleModal').modal('show');
    }
</script>