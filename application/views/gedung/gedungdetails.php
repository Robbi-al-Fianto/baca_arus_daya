<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->

    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?> <a href="<?= site_url('gedung/export_excel/' . $gedung__id); ?>" type="button" class="btn btn-success">Export Excel</a></h1>

    <div class="row">
        <div class="col-lg-8">


            <?= $this->session->flashdata('message'); ?>

            <h5>Gedung : <?= $listgedung['nama_gedung']; ?></h5>


            <table class="table table-striped table-bordered mydatatable">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date(Y,M,D)Time</th>
                        <th scope="col">Current</th>
                        <th scope="col">Energy</th>
                        <th scope="col">Frequency</th>
                        <th scope="col">Power</th>
                        <th scope="col">PowerFactor</th>
                        <th scope="col">Voltage</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($data_sdg as $d) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $d['created_at']; ?></td>
                            <td><?= $d['Current']; ?></td>
                            <td><?= $d['Energy']; ?></td>
                            <td><?= $d['Frequency']; ?></td>
                            <td><?= $d['Power']; ?></td>
                            <td><?= $d['PowerFactor']; ?></td>
                            <td><?= $d['Voltage']; ?></td>

                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>


        </div>
    </div>



</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $('.mydatatable').DataTable();
</script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>