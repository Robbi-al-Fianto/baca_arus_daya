<!-- Begin Page Content -->
<div class="container-fluid" id="realtime">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div class="row">
        <div class="col-lg-8">

            <?= form_error('menu', '<div class="alert alert-danger" role="alert">
                    Gedung Field is Required</div>') ?>

            <?= $this->session->flashdata('message'); ?>

            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newGedungModal">Add New Gedung</a>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Gedung Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">Code</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($listGedung as $keyNum => $G) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $G['nama_gedung']; ?></td>
                            <td><?= $G['address']; ?></td>
                            <td><?= $G['etc']; ?></td>
                            <td>
                                <?php $relativePath = 'assets/img/gedung/' . $G['foto']; ?>
                                <?php $fullPath     = FCPATH . $relativePath; ?>
                                <?php $isFileExist  = $G['foto'] && file_exists($fullPath) && is_file($fullPath); ?>
                                <?php if ($isFileExist) : ?>
                                    <a href="<?= base_url($relativePath); ?>" target="_blank"><?= $G['foto']; ?></a>
                                <?php else : ?>
                                    <?= $G['foto']; ?>
                                <?php endif; ?>
                            </td>
                            <td><?= $G['created_at']; ?></td>
                            <td>
                                <button onclick="update_gedung('<?= $keyNum; ?>')" class="badge badge-success">edit</button>
                                <button onclick="delete_gedung('<?= $keyNum; ?>')" class=" badge badge-danger">delete</button>
                                <button type="button" onclick="location.href='<?= base_url('gedung/gedungdetails/') . $G['id']; ?>'" class="badge badge-warning">history</button>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>


        </div>
    </div>


</div>
</div>
<!-- /.container-fluid -->


<!-- End of Main Content -->



<!-- NEW Modal -->
<div class="modal fade" id="newGedungModal" tabindex="-1" role="dialog" aria-labelledby="newGedungModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newGedungModalLabel">Add Gedung</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= site_url('gedung/addgedung'); ?>" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="add_nama_gedung">Nama Gedung<sup class="text-danger">*</sup></label>
                        <input type="text" class="form-control" id="add_nama_gedung" name="nama_gedung" placeholder="Gedung name" required>
                    </div>

                    <div class="form-group">
                        <label for="add_address">Alamat<sup class="text-danger">*</sup></label>
                        <input type="text" class="form-control" id="add_address" name="address" placeholder="Address" required>
                    </div>

                    <div class="form-group">
                        <label for="add_etc">Lain-lain<sup class="text-danger">*</sup></label>
                        <textarea class="form-control" id="add_etc" name="etc" placeholder="etc" required></textarea>
                    </div>

                    <div class="form-group">
                        <label for="gedung__foto__add">Foto Gedung<sup class="text-danger">*</sup></label>
                        <input type="file" class="form-control" id="gedung__foto__add" name="gedung__foto__add" placeholder="Gedung Photo" required>
                    </div>

                    <div class="form-group">
                        <span><sup class="text-danger">*</sup> Wajib Diisi</span>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Update Modal -->
<div class="modal fade" id="updateGedungModal" tabindex="-1" role="dialog" aria-labelledby="updateGedungModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateGedungModalLabel">Edit Gedung</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= site_url('gedung/updategedung'); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="edit_id">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="edit_nama_gedung">Nama Gedung<sup class="text-danger">*</sup></label>
                        <input type="text" class="form-control" id="edit_nama_gedung" name="edit_nama_gedung" placeholder="" required>
                    </div>
                    <div class="form-group">
                        <label for="edit_address">Alamat<sup class="text-danger">*</sup></label>
                        <input type="text" class="form-control" id="edit_address" name="edit_address" placeholder="Alamat" required>
                    </div>
                    <div class="form-group">
                        <label for="edit_etc">Lain-lain<sup class="text-danger">*</sup></label>
                        <input type="text" class="form-control" id="edit_etc" name="edit_etc" placeholder="etc" required>
                    </div>
                    <div class="form-group">
                        <label for="gedung__foto__edit">Foto Gedung<sup class="text-danger">*</sup></label>
                        <input type="file" class="form-control" id="gedung__foto__edit" name="gedung__foto__edit" placeholder="Foto Baru">
                    </div>
                    <div class="form-group">
                        <label>Foto lama</label>
                        <img class="img-fluid" style="max-height:200px;" id="gedung__foto__lama">
                    </div>
                    <div class="form-group">
                        <span><sup class="text-danger">*</sup> Wajib Diisi</span>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal Delete Product -->
<div class="modal fade" id="deleteGedungModal" tabindex="-1" role="dialog" aria-labelledby="deleteGedungModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteGedungModalLabel">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger">
                    Are You Sure To Delete This Gedung?
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <form action="<?php echo base_url('gedung/deletegedung'); ?>" method="post">
                    <input type="hidden" id="delete_id" name="id" value="<?php echo trim($G['id']); ?>">
                    <button type="submit" class="btn btn-primary btn-delete">Yes</button>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    let base_url = '<?= base_url(); ?>';
    let datagedung = JSON.parse(`<?= json_encode($listGedung) ?>`);

    function update_gedung(keyNum) {
        let listGedung = datagedung[keyNum];
        $('[name="edit_id"]').val(listGedung['id']);
        $('#edit_nama_gedung').val(listGedung['nama_gedung']);
        $('#edit_address').val(listGedung['address']);
        $('#edit_etc').val(listGedung['etc']);
        if (listGedung['foto']) {
            $('#gedung__foto__lama').attr('src', `${base_url}assets/img/gedung/` + listGedung['foto']);
            $('#gedung__foto__lama').attr('alt', listGedung['foto']);
        };

        $('#updateGedungModal').modal('show');
    }

    function delete_gedung(keyNum) {
        let listGedung = datagedung[keyNum];
        let id = datagedung['id'];
        $('[name="delete_id"]').val(listGedung['id']);

        $('#deleteGedungModal').modal('show');
    }
</script>