<!DOCTYPE html>
<html lang="en">

<head>
    <title>Monitoring &mdash; Website</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Oswald:400,700|Dancing+Script:400,700|Muli:300,400" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assetsfe/')  ?>fonts/icomoon/style.css">

    <link rel="stylesheet" href="<?= base_url('assetsfe/')  ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('assetsfe/')  ?>css/jquery-ui.css">
    <link rel="stylesheet" href="<?= base_url('assetsfe/')  ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url('assetsfe/')  ?>css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?= base_url('assetsfe/')  ?>css/owl.theme.default.min.css">

    <link rel="stylesheet" href="<?= base_url('assetsfe/')  ?>css/jquery.fancybox.min.css">

    <link rel="stylesheet" href="<?= base_url('assetsfe/')  ?>css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="<?= base_url('assetsfe/')  ?>fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="<?= base_url('assetsfe/')  ?>css/aos.css">
    <link href="<?= base_url('assetsfe/')  ?>css/jquery.mb.YTPlayer.min.css" media="all" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="<?= base_url('assetsfe/')  ?>css/style.css">



</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    <div class="site-wrap">