<div class="site-mobile-menu site-navbar-target">
    <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
        </div>
    </div>
    <div class="site-mobile-menu-body"></div>
</div>



<div class="header-top bg-light">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-6 col-lg-3">
                <a href="index.html">
                    <img src="<?= base_url('assetsfe/') ?>images/logo1.png" alt="Image" class="img-fluid">
                    <!-- <strong>Water</strong>Boat -->
                </a>
            </div>
            <div class="col-lg-3 d-none d-lg-block">

                <div class="quick-contact-icons d-flex">
                    <div class="icon align-self-start">
                        <span class="icon-location-arrow text-primary"></span>
                    </div>
                    <div class="text">
                        <span class="h4 d-block">Surabaya</span>
                        <span class="caption-text">Jl. Perak Timur No.610</span>
                    </div>
                </div>

            </div>
            <div class="col-lg-3 d-none d-lg-block">
                <div class="quick-contact-icons d-flex">
                    <div class="icon align-self-start">
                        <span class="icon-phone text-primary"></span>
                    </div>
                    <div class="text">
                        <span class="h4 d-block"> (031) 3298631</span>
                        <span class="caption-text">Toll free</span>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 d-none d-lg-block">
                <div class="quick-contact-icons d-flex">
                    <div class="icon align-self-start">
                        <span class="icon-envelope text-primary"></span>
                    </div>
                    <div class="text">
                        <span class="h4 d-block">hc.services@pelindo.co.id</span>
                        <span class="caption-text">Jawa Timur 60165</span>
                    </div>
                </div>
            </div>

            <div class="col-6 d-block d-lg-none text-right">
                <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a>
            </div>
        </div>
    </div>




    <div class="site-navbar py-2 js-sticky-header site-navbar-target d-none pl-0 d-lg-block" role="banner">

        <div class="container">
            <div class="d-flex align-items-center">

                <div class="mx-auto">
                    <nav class="site-navigation position-relative text-right" role="navigation">
                        <ul class="site-menu main-menu js-clone-nav mr-auto d-none pl-0 d-lg-block">
                            <li>
                                <a href="<?= site_url('frontend'); ?>" class="nav-link text-left <?= preg_match('/^frontend$/', uri_string()) ? 'active' : ''; ?>">Home</a>
                            </li>
                            <li>
                                <a href="<?= site_url('frontend/about'); ?>" class="nav-link text-left <?= preg_match('/^frontend\/about/', uri_string()) ? 'active' : ''; ?>">About Us</a>
                            </li>
                            <li>
                                <a href="<?= site_url('frontend/contact'); ?>" class="nav-link text-left <?= preg_match('/^frontend\/contact/', uri_string()) ? 'active' : ''; ?>">Contact</a>
                            </li>
                            <li>
                                <a href="<?= site_url('frontend/gedung'); ?>" class="nav-link text-left <?= preg_match('/^frontend\/gedung/', uri_string()) ? 'active' : ''; ?>">Gedung</a>
                            </li>
                            <li>
                                <a href="<?= site_url('auth'); ?>">Login</a>
                            </li>
                        </ul>
                        </ul>
                    </nav>

                </div>

            </div>
        </div>

    </div>

</div>