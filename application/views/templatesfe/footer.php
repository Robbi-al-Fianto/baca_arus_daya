<div class="footer bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mx-auto">
                <h1 class="mx-auto"><img src="<?= base_url('assetsfe/')  ?>images/logo1.png" alt="Image" class="img-fluid"></h1>
            </div>

        </div>

    </div>
</div>


</div>
<!-- .site-wrap -->


<!-- loader -->

<script src="<?= base_url('assetsfe/')  ?>js/jquery-3.3.1.min.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/jquery-migrate-3.0.1.min.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/jquery-ui.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/popper.min.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/bootstrap.min.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/owl.carousel.min.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/jquery.stellar.min.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/jquery.countdown.min.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/jquery.easing.1.3.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/aos.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/jquery.fancybox.min.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/jquery.sticky.js"></script>
<script src="<?= base_url('assetsfe/')  ?>js/jquery.mb.YTPlayer.min.js"></script>




<script src="<?= base_url('assetsfe/')  ?>js/main.js"></script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/62b82858b0d10b6f3e795a4c/1g6flr6vr';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->

</body>

</html>