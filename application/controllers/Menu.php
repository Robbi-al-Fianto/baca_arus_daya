<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    public function index()
    {
        $data['title'] = 'Menu Management';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();


        $this->form_validation->set_rules('menu', 'Menu', 'required');

        if ($this->form_validation->run() == false) {

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/index', $data);
            $this->load->view('templates/footer', $data);
        } else {
            $this->db->insert('user_menu', ['menu' => $this->input->post('menu')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    New Menu Added</div>');
            redirect('menu');
        }
    }


    public function submenu()
    {
        $data['title'] = 'Submenu Management';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->model('Menu_model', 'menu');

        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'Icon', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/submenu', $data);
            $this->load->view('templates/footer', $data);
        } else {
            $data = [
                'title' => $this->input->post('title'),
                'menu_id' => $this->input->post('menu_id'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active')
            ];
            $this->db->insert('user_sub_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            New Sub Menu Added</div>');
            redirect('menu/submenu');
        }
    }
    function update()
    {

        $id   = $this->input->post('edit_id');
        $menu = $this->input->post('edit_menu');
        $id   = trim($id);
        $menu = trim($menu);

        $data = array(
            'menu' => $menu
        );

        $this->db->where('id', $id);
        $this->db->update('user_menu', $data);

        $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
        Menu Updated</div>');

        return redirect('Menu');
    }
    function delete()
    {
        $id = $this->input->post('id');
        $this->db->delete('user_menu', array('id' => $id));

        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        Menu Deleted</div>');

        return redirect('Menu');
    }

    function updatesubmenu()
    {
        // $data['subMenu'] = $this->menu->getSubMenu();
        // $data['menu'] = $this->db->get('user_menu')->result_array();

        // $this->load->model('Menu_model', 'menu');

        $id       = $this->input->post('edit_id');
        $menu     = $this->input->post('edit_menu_id');
        $title    = $this->input->post('edit_title');
        $url      = $this->input->post('edit_url');
        $icon     = $this->input->post('edit_icon');
        $id       = trim($id);
        $title    = trim($title);
        $menu     = trim($menu);
        $url      = trim($url);
        $icon     = trim($icon);

        $data = array(
            'menu_id'   => $menu,
            'title'     => $title,
            'url'       => $url,
            'icon'      => $icon
        );

        $this->db->where('id', $id);
        $this->db->update('user_sub_menu', $data);

        $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
        Submenu Updated</div>');

        redirect('Menu/submenu');
    }
    function deletesubmenu()
    {
        $id = $this->input->post('id');
        $this->db->delete('user_sub_menu', array('id' => $id));

        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        Submenu Deleted</div>');

        redirect('Menu/submenu');
    }
}
