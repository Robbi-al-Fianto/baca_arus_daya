<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Frontend extends CI_Controller
{
    public function index()
    {
        $data['title'] = 'Home';

        $this->load->view('templatesfe/header', $data);
        $this->load->view('templatesfe/topbar');
        $this->load->view('frontend/index');
        $this->load->view('templatesfe/footer');
    }
    public function about()
    {
        $data['title'] = 'Aboyt Us';

        $this->load->view('templatesfe/header', $data);
        $this->load->view('templatesfe/topbar');
        $this->load->view('frontend/about');
        $this->load->view('templatesfe/footer');
    }
    public function contact()
    {
        $data['title'] = 'Contact';

        $this->load->view('templatesfe/header', $data);
        $this->load->view('templatesfe/topbar');
        $this->load->view('frontend/contact');
        $this->load->view('templatesfe/footer');
    }

    public function gedung()
    {
        $this->load->model('Gedung_m');

        $data['title'] = 'Daftar Gedung';

        $data['list_gedung'] = $this->Gedung_m->listGedung();

        $this->load->view('templatesfe/header', $data);
        $this->load->view('templatesfe/topbar');
        $this->load->view('frontend/gedung');
        $this->load->view('templatesfe/footer');
    }

    public function send_email()
    {

        $fname    = $this->input->post('fname');
        $lname    = $this->input->post('lname');
        $eaddress = $this->input->post('eaddress');
        $subject  = $this->input->post('subject');
        $message  = $this->input->post('message');

        $this->email->from($this->email->smtp_user, "$lname, $fname"); /* EMAIL CONFIG */
        $this->email->to($this->email->smtp_user); /* EMAIL PRIBADI */
        $this->email->cc($eaddress);

        $this->email->subject("MONITORING: $subject");
        $this->email->message($message);
        $isSuccess = $this->email->send();



        if ($isSuccess) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="success">
            Berhasil Terkirim</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Gagal Terkirim</div>');
        };
        return redirect('frontend/contact');
    }
}
