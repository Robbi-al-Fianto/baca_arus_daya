<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Daya_arus', 'daya_arus');
        $this->load->model('Admin_m', 'admin');
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $data['avg'] = $this->db->query(
            "SELECT current_timestamp(), created_at, AVG(Voltage), AVG(Current), AVG(Frequency) FROM sdg__1 WHERE created_at"
        )->result_array();

        // $data['getalldaya'] = $this->daya_arus->get_all_daya();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/index', $data);
        $this->load->view('templates/footer', $data);
    }

    public function role()
    {
        $data['role'] = $this->db->get('user_role')->result_array();

        $data['title'] = 'Role';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('role', 'Role', 'required');

        if ($this->form_validation->run() == false) {

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/role', $data);
            $this->load->view('templates/footer', $data);
        } else {
            $this->db->insert('user_role', ['role' => $this->input->post('role')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    New Role Added</div>');
            redirect('admin/role');
        }
    }
    function updaterole()
    {

        $id   = $this->input->post('edit_id');
        $role = $this->input->post('edit_role');
        $id   = trim($id);
        $role = trim($role);

        $data = array(
            'role' => $role
        );

        $this->db->where('id', $id);
        $this->db->update('user_role', $data);

        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        Role Deleted</div>');

        return redirect('admin/role');
    }
    function deleterole()
    {

        $id = $this->input->post('id');
        $this->db->delete('user_role', array('id' => $id));

        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        Role Deleted</div>');

        return redirect('admin/role');
    }


    public function roleAccess($role_id)
    {

        $data['title'] = 'Role Access';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $data['role'] = $this->db->get_where('user_role', ['id' => $role_id])->row_array();

        // $this->db->where('id !=', 1);
        $this->db->where('id !=', 1);
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/role-access', $data);
        $this->load->view('templates/footer', $data);
    }

    public function changeaccess()
    {
        $role_id = $this->input->post('roleId');
        $menu_id = $this->input->post('menuId');

        $data = [
            'role_id' => $role_id,
            'menu_id' => $menu_id
        ];
        $result = $this->db->get_where('user_access_menu', $data);

        if ($result->num_rows() < 1) {
            $this->db->insert('user_access_menu', $data);
        } else {
            $this->db->delete('user_access_menu', $data);
        }
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        Access Changed    
        </div>');
    }
    // public function ambildata()
    // {
    //     $data['getalldaya'] = $this->daya_arus->get_all_daya();
    //     echo json_encode($data);
    // }

    public function sendmysql()
    {
        $data['avg'] = $this->db->query(
            "SELECT AVG(Voltage), AVG(Current), AVG(Frequency) FROM sdg__1 WHERE DATE(created_at)"
        )->result_array();

        // $data['avg'] = $this->db->query(
        //     "SELECT AVG(Voltage), AVG(Current), AVG(Frequency) FROM sdg__1 WHERE DATE(created_at) = curdate()"
        // )->result_array();
        $ii = 1;
        foreach ($data['avg'] as $a) :
            $ii;
            $a['AVG(Voltage)'];
            $a['AVG(Current)'];
            $a['AVG(Frequency)'];

            $html1 =

                '
                <h1>Rata Rata Daya Perhari</h1>      
            <div class="row">
                <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Average Current</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' .  number_format((float)$a['AVG(Current)'], 3, '.', '') . '</div></div>
                            <div class="col-auto">
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Average Frequency</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' . number_format((float)$a['AVG(Frequency)'], 2, '.', '') . '</div></div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Average Voltage</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' . number_format((float)$a['AVG(Voltage)'], 2, '.', '') . '</div></div>
                    <div class="col-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    ';
            $ii++;
        endforeach;
        echo $html1;
    }
    public function senddatabase()
    {

        $this->load->model('Gedung_m', 'gedung');
        $gedungfb = $this->gedung->getFB_gedung();
        $gedungs = $this->admin->get_gedung();


        if ($gedungfb['StatusValue'] == 1) {
            $html = '<button class="btn btn-success" type="submit">Device Connected</button>
            <br>';
            echo $html;
            if ($gedungfb['Current'] == 0) {
                $html =

                    '
                    
            <div class="row">
                <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Current</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Current'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Ampere (A)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            } else {
                $html =

                    '
        <div class="row">
            <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Current</div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Current'] . '</div></div>
                        <div class="col-auto">
                        <i class="fa-1x text-gray-300">Ampere (A)</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
                echo $html;
            }
            if ($gedungfb['Energy'] == 0) {
                $html =
                    '<div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1"> Energy</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Energy'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Watt-hour (Wh)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            } else {
                $html =
                    '<div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1"> Energy</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Energy'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Watt-hour (Wh)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            }


            if ($gedungfb['Frequency'] == 0) {
                $html =

                    '   <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1"> Frequency</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Frequency'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Hertz (Hz)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            } else {
                $html =

                    '   <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1"> Frequency</div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Frequency'] . '</div></div>
                        <div class="col-auto">
                        <i class="fa-1x text-gray-300">Hertz (Hz)</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
                echo $html;
            }
            if ($gedungfb['Power'] == 0) {
                $html =
                    '
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1"> Power</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Power'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Watt (W)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            } else {
                $html =
                    '
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1"> Power</div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Power'] . '</div></div>
                        <div class="col-auto">
                        <i class="fa-1x text-gray-300">Watt (W)</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
                echo $html;
            }
            if ($gedungfb['PowerFactor'] == 0) {
                $html =

                    '
    
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1"> Power Factor</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['PowerFactor'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Kilowatt-hour (kWh)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            } else {
                $html =  '
                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="card border-left-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1"> Power Factor</div>
                                    <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['PowerFactor'] . '</div></div>
                                <div class="col-auto">
                                <i class="fa-1x text-gray-300">Kilowatt-hour (kWh)</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
                echo $html;
            }
            if ($gedungfb['Voltage'] == 0) {
                $html =
                    '
    
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Voltage</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Voltage'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Volt (V)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            } else {
                $html =
                    '

        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Voltage</div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Voltage'] . '</div></div>
                        <div class="col-auto">
                        <i class="fa-1x text-gray-300">Volt (V)</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
                echo $html;
            }

            '
            </div>
            <br>';
            // echo $html;
        } else {

            $html =

                '
                <button class="btn btn-danger" type="submit">Device Not Connected</button>
              

    <div class="row">
        <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Current</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' .  $gedungfb['Current'] . '</div></div>
                    <div class="col-auto">
                    <i class="fa-1x text-gray-300">Ampere (A)</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Energy</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Energy'] . '</div></div>
                    <div class="col-auto">
                    <i class="fa-1x text-gray-300">Watt-hour (Wh)</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Frequency</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Frequency'] . '</div></div>
                    <div class="col-auto">
                    <i class="fa-1x text-gray-300">Hertz (Hz)</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Power</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Power'] . '</div></div>
                    <div class="col-auto">
                    <i class="fa-1x text-gray-300">Watt (W)</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Power Factor</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['PowerFactor'] . '</div></div>
                    <div class="col-auto">
                    <i class="fa-1x text-gray-300">Kilowatt-hour (kWh)</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Voltage</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Voltage'] .  '</div></div>
                    <div class="col-auto">
                    <i class="fa-1x text-gray-300">Volt (V)</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    </div>
    <br>';
            echo $html;
        }
            // $html =

            //     '
            //     <div class="row">
            //         <div class="col-xl-4 col-md-6 mb-4">
            //         <div class="card border-left-primary shadow h-100 py-2">
            //             <div class="card-body">
            //                 <div class="row no-gutters align-items-center">
            //                     <div class="col mr-2">
            //                         <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Current</div>
            //                         <div class="h1 mb-0 font-weight-bold text-gray-800">' .  isset($gedungfb['Current']) . '</div></div>
            //                     <div class="col-auto">
            //                     <i class="fa-2x text-gray-300">Electric Current (I)</i>
            //                     </div>
            //                 </div>
            //             </div>
            //         </div>
            //     </div>


            //     <div class="col-xl-4 col-md-6 mb-4">
            //         <div class="card border-left-primary shadow h-100 py-2">
            //             <div class="card-body">
            //                 <div class="row no-gutters align-items-center">
            //                     <div class="col mr-2">
            //                         <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Energy</div>
            //                         <div class="h1 mb-0 font-weight-bold text-gray-800">' . isset($gedungfb['Energy']) . '</div></div>
            //                     <div class="col-auto">
            //                     <i class="fa-2x text-gray-300">Energy (E)</i>
            //                     </div>
            //                 </div>
            //             </div>
            //         </div>
            //     </div>

            //     <div class="col-xl-4 col-md-6 mb-4">
            //         <div class="card border-left-primary shadow h-100 py-2">
            //             <div class="card-body">
            //                 <div class="row no-gutters align-items-center">
            //                     <div class="col mr-2">
            //                         <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Frequency</div>
            //                         <div class="h1 mb-0 font-weight-bold text-gray-800">' . isset($gedungfb['Frequency']) . '</div></div>
            //                     <div class="col-auto">
            //                     <i class="fa-2x text-gray-300">Frequency (f)</i>
            //                     </div>
            //                 </div>
            //             </div>
            //         </div>
            //     </div>

            //     <div class="col-xl-4 col-md-6 mb-4">
            //         <div class="card border-left-primary shadow h-100 py-2">
            //             <div class="card-body">
            //                 <div class="row no-gutters align-items-center">
            //                     <div class="col mr-2">
            //                         <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Power</div>
            //                         <div class="h1 mb-0 font-weight-bold text-gray-800">' . isset($gedungfb['Power']) . '</div></div>
            //                     <div class="col-auto">
            //                     <i class="fa-2x text-gray-300">Electric Power (P)</i>
            //                     </div>
            //                 </div>
            //             </div>
            //         </div>
            //     </div>

            //     <div class="col-xl-4 col-md-6 mb-4">
            //         <div class="card border-left-primary shadow h-100 py-2">
            //             <div class="card-body">
            //                 <div class="row no-gutters align-items-center">
            //                     <div class="col mr-2">
            //                         <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Power Factor</div>
            //                         <div class="h1 mb-0 font-weight-bold text-gray-800">' . isset($gedungfb['PowerFactor']) . '</div></div>
            //                     <div class="col-auto">
            //                     <i class="fa-2x text-gray-300">Power factor (PF)</i>
            //                     </div>
            //                 </div>
            //             </div>
            //         </div>
            //     </div>

            //     <div class="col-xl-4 col-md-6 mb-4">
            //         <div class="card border-left-primary shadow h-100 py-2">
            //             <div class="card-body">
            //                 <div class="row no-gutters align-items-center">
            //                     <div class="col mr-2">
            //                         <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Voltage</div>
            //                         <div class="h1 mb-0 font-weight-bold text-gray-800">' . isset($gedungfb['Voltage']) . '</div></div>
            //                     <div class="col-auto">
            //                     <i class="fa-2x text-gray-300">Voltage (V)</i>
            //                     </div>
            //                 </div>
            //             </div>
            //         </div>
            //     </div>


            //     </div>
            //     <br>'
            // <div class="row">
            // <div class="col-sm-4 mb-4">
            // <div class="card text-dark bg-light">
            // <div class="card-body ">

            // <h5 class="card-title">From Firebase</h5>

            // <div class="list-group list-group-flush">
            // <a href="#" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-secondary disabled">Current <span class="badge badge-primary badge-pill">' . $gedungfb['Current'] . '</span></a>
            // <a href="#" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-secondary disabled">Energy <span class="badge badge-primary badge-pill">' . $gedungfb['Energy'] . '</span></a>
            // <a href="#" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-secondary disabled">Frequency <span class="badge badge-primary badge-pill">'  . $gedungfb['Frequency'] . '</span></a>
            // <a href="#" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-secondary disabled">Power <span class="badge badge-primary badge-pill">' . $gedungfb['Power'] .   '</span></a>
            // <a href="#" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-secondary disabled">Power Factor <span class="badge badge-primary badge-pill">' . $gedungfb['PowerFactor'] . '</span></a>
            // <a href="#" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-secondary disabled">Voltage <span class="badge badge-primary badge-pill">' . $gedungfb['Voltage'] .  '</span></a>
            // </div>

            // </div>
            // </div>
            // </div>
            // </div>



            // <li class="list-group-item">Energy       : ' . $gedungfb['Energy'] . '</li>
            // <li class="list-group-item">Frequency      : ' . $gedungfb['Frequency'] . '</li>
            // <li class="list-group-item">Power   : ' . $gedungfb['Power'] .  '</li>
            // <li class="list-group-item">PowerFactor : ' . $gedungfb['PowerFactor'] . '</li>
            // <li class="list-group-item">Voltage     : ' . $gedungfb['Voltage'] . '</li>
            // <li class="list-group-item">Updated_at  : ' . (isset($sdg['created_at'])  ? $sdg['created_at']  : ' ') .  '</li>




        ;
        foreach ($gedungs as $key => $gedung) {
            $idGedung = $gedung['id'];
            $sdgGedung = $this->admin->getsdggedung($idGedung);
            $gedungs[$key]['sdg'] = $sdgGedung;

            $html .= $this->admin->cardRender($gedungs[$key]);
        }

        // echo $html;
        // echo json_encode($gedungs);
    }
}
