<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Building extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Daya_arus', 'daya_arus');
    }

    public function index()
    {
        $data['title'] = 'Building';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('building/index', $data);
        $this->load->view('templates/footer', $data);
    }

    public function electrical()
    {
        $data['id_gedung'] = $this->db->get('daya_listrik')->result_array();

        $data['getalluser'] = $this->daya_arus->getalluser();

        $data['title'] = 'Electrical';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('building/electrical', $data);
        $this->load->view('templates/footer', $data);
    }
    public function electricalDetails($building_name)
    {

        $data['title'] = 'Electric Details';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();


        $data['id_gedung'] = $this->db->get_where('daya_listrik', ['id_gedung' => $building_name])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('building/electrical_details', $data);
        $this->load->view('templates/footer', $data);
    }

    // START CRUD DAYA TEGANGAN
    function get_daya()
    {
        $data = $this->daya_arus->get_daya()->result();
        echo json_encode($data);
    }
    function create()
    {

        $id_gedung = $this->input->post('id_gedung', TRUE);
        $voltage = $this->input->post('voltage', TRUE);
        $frekuensi = $this->input->post('frekuensi', TRUE);
        $arus = $this->input->post('arus', TRUE);
        $this->daya_arus->insert_daya($id_gedung, $voltage, $frekuensi, $arus);

        require_once(APPPATH . 'views/vendor/autoload.php');
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );
        $pusher = new Pusher\Pusher(
            'f02b5d8a1b4f1d7524de',
            '29eff955abafd5bf02dd',
            '1329122',
            $options
        );

        $data['message'] = 'success';
        $pusher->trigger('my-channel', 'my-event', $data);
        redirect('admin');
    }
    function update()
    {
        $id = $this->input->post('id', TRUE);
        $id_gedung = $this->input->post('id_gedung', TRUE);
        $voltage = $this->input->post('voltage', TRUE);
        $frekuensi = $this->input->post('frekuensi', TRUE);
        $arus = $this->input->post('arus', TRUE);
        $this->daya_arus->update_daya($id, $id_gedung, $voltage, $frekuensi, $arus);

        require_once(APPPATH . 'views/vendor/autoload.php');
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );
        $pusher = new Pusher\Pusher(
            'f02b5d8a1b4f1d7524de',
            '29eff955abafd5bf02dd',
            '1329122',
            $options
        );

        $data['message'] = 'success';
        $pusher->trigger('my-channel', 'my-event', $data);
    }

    function delete()
    {
        $id = $this->input->post('id', TRUE);
        $this->daya_arus->delete_daya($id);

        require_once(APPPATH . 'views/vendor/autoload.php');
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );
        $pusher = new Pusher\Pusher(
            'f02b5d8a1b4f1d7524de',
            '29eff955abafd5bf02dd',
            '1329122',
            $options
        );

        $data['message'] = 'success';
        $pusher->trigger('my-channel', 'my-event', $data);
    }

    // END CRUD DAYA TEGANGAN

}
