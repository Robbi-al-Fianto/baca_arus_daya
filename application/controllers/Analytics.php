<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Analytics extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Daya_arus', 'daya_arus');
        $this->load->model('Admin_m', 'admin');
    }

    public function index()
    {
        $data['title'] = 'Gedung Analytics';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        // $data['getalldaya'] = $this->daya_arus->get_all_daya();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('analytics/index', $data);
        $this->load->view('templates/footer', $data);
    }
}
