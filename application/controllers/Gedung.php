<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;

class Gedung extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Gedung_m', 'gedung');
    }

    public function index()
    {
        $data['title'] = 'Gedung Management';
        $data['user']  = $this->db->get_where('user', [
            'email' => $this->session->userdata('email')
        ])->row_array();
        $data['listGedung'] = $this->gedung->listGedung('*', [
            'is_active' => '1'
        ]);

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('gedung/index', $data);
        $this->load->view('templates/footer', $data);
    }

    public function addgedung()
    {
        //cek gambar
        $nameInputFIle = 'gedung__foto__add';
        $gedung__photo = $_FILES[$nameInputFIle];

        $nama_file_photo = null;
        if ($gedung__photo['error'] == UPLOAD_ERR_OK) { /* Jika user meng-upload file */
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']      = '5000';
            $config['upload_path']   = './assets/img/gedung/';
            $config['file_name']     = time() . '_' . rand(1000000, 9999999) . '_' . $gedung__photo['name']; /* memberi nama random */

            $this->load->library('upload', $config);

            if ($this->upload->do_upload($nameInputFIle)) {
                $data = $this->upload->data();

                $nama_file_photo = $data['file_name'];
            } else { /* Jika gagal menyimpan file yang dipload */
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $this->upload->display_errors() . '</div>');
                redirect(site_url('gedung'));
            };
        } elseif ($gedung__photo['error'] == UPLOAD_ERR_NO_FILE) { /* Jika tidak ada file yang diupload */
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Fott Wajib Diisi</div>');
            redirect(site_url('gedung'));
        };

        $data_insert = [
            'nama_gedung' => $this->input->post('nama_gedung'),
            'address'     => $this->input->post('address'),
            'etc'         => $this->input->post('etc'),
            'foto'        => $nama_file_photo,
        ];
        $this->gedung->create($data_insert);

        $this->session->set_flashdata('message', '<div Congratulation! class="alert alert-success" role="alert">
        Gedung berhasil dibuat! </div>');
        redirect('gedung');
    }

    public function updategedung()
    {

        $id = trim($this->input->post('edit_id'));

        $oldData        = $this->gedung->getById($id);
        $relativePath   = 'assets/img/gedung/' . $oldData['foto'];
        $fullPath       = FCPATH . $relativePath;
        $isOldFileExist = $oldData['foto'] && file_exists($fullPath) && is_file($fullPath);

        //cek gambar
        $nameInputFIle = 'gedung__foto__edit';
        $gedung__photo = $_FILES[$nameInputFIle];

        $nama_file_photo = null;
        if ($gedung__photo['error'] == UPLOAD_ERR_OK) { /* Jika user meng-upload file */
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']      = '5000';
            $config['upload_path']   = './assets/img/gedung/';
            $config['file_name']     = time() . '_' . rand(1000000, 9999999) . '_' . $gedung__photo['name']; /* memberi nama random */

            $this->load->library('upload', $config);

            if ($this->upload->do_upload($nameInputFIle)) {
                $data = $this->upload->data();

                if ($isOldFileExist) { /* jika file lama ada, dan user menginput file baru, maka file lama dihapus */
                    unlink($fullPath);
                };

                $nama_file_photo = $data['file_name'];
            } else { /* Jika gagal menyimpan file yang dipload */
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $this->upload->display_errors() . '</div>');
                redirect(site_url('gedung'));
            };
        } elseif ($gedung__photo['error'] == UPLOAD_ERR_NO_FILE) {
            /* Jika tidak ada file yang diupload, maka tidak melakukan apa-apa karena upload file tidak wajib */
        };

        $data = [
            'nama_gedung' => trim($this->input->post('edit_nama_gedung')),
            'address'     => trim($this->input->post('edit_address')),
            'etc'         => trim($this->input->post('edit_etc'))
        ];

        if ($nama_file_photo) { /* Jika ada file baru  */
            $data['foto'] = $nama_file_photo;
        };

        $this->db->where('id', $id)->update('gedung', $data);

        $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
        Gedung Updated</div>');

        return redirect('gedung');
    }

    public function deletegedung()
    {
        $id = $this->input->post('id');

        $this->gedung->destroy($id);

        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        Gedung Deleted</div>');

        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        Gedung Deleted</div>');

        return redirect('gedung');
    }

    public function detailsgedung($id)
    {
        $tabelName = 'sdg__' . $id;
        $data['gedung__id'] = $id;
        $data['data_sdg'] = $this->db->query(
            "SELECT * FROM $tabelName ORDER BY created_at
        "
        )->result_array();

        $data['avg'] = $this->db->query(
            "SELECT current_timestamp(), created_at, AVG(Voltage) FROM $tabelName WHERE created_at"
        )->result_array();




        $data['title'] = 'Gedung History';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $data['listgedung'] = $this->db->get_where('gedung', ['id' => $id])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('gedung/gedungdetails', $data);
        $this->load->view('templates/footer', $data);
    }

    public function gedungfirebase()
    {
        // $firebase = $this->firebase->init();
        // $db = $firebase->getDatabase();
        // $systemData = $db->getReference('System Data');
        // $value = $systemData->getValue();

        $value = $this->gedung->getFB_gedung();

        $data['title'] = 'Gedung Firebase';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();
        $data['data'] = $value;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('gedung/gedungfirebase', $data);
        $this->load->view('templates/footer', $data);
    }

    public function export_excel($gedung__id)
    {
        error_reporting(32767);
        $tabelName = 'sdg__' . $gedung__id;
        $data_gedung = $this->db->get_where('gedung', [
            'gedung.id' => $gedung__id,
        ])->row_array();
        $data_sdg = $this->db->get($tabelName)->result_array();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $row = 1;

        $sheet->setCellValue("A$row", 'Nama Gedung:');
        $sheet->setCellValue("B$row", $data_gedung['nama_gedung']);
        $row++;

        $sheet->setCellValue("A$row", 'Alamat:');
        $sheet->setCellValue("B$row", $data_gedung['address']);
        $row++;

        $sheet->setCellValue("A$row", 'Lain-lain:');
        $sheet->setCellValue("B$row", $data_gedung['etc']);
        $row++;
        $row++;

        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ];
        $sheet->setCellValue("A$row", 'No.')->getStyle("A$row")->applyFromArray($styleArray);
        $sheet->setCellValue("B$row", 'Voltage')->getStyle("B$row")->applyFromArray($styleArray);
        $sheet->setCellValue("C$row", 'Current')->getStyle("C$row")->applyFromArray($styleArray);
        $sheet->setCellValue("D$row", 'Power')->getStyle("D$row")->applyFromArray($styleArray);
        $sheet->setCellValue("E$row", 'Energy')->getStyle("E$row")->applyFromArray($styleArray);
        $sheet->setCellValue("F$row", 'Frequency')->getStyle("F$row")->applyFromArray($styleArray);
        $sheet->setCellValue("G$row", 'Power Factor')->getStyle("G$row")->applyFromArray($styleArray);
        $sheet->setCellValue("H$row", 'Status Value')->getStyle("H$row")->applyFromArray($styleArray);
        $sheet->setCellValue("I$row", 'Created At')->getStyle("I$row")->applyFromArray($styleArray);
        $row++;

        $no = 1;
        foreach ($data_sdg as $value) {
            $sheet->setCellValue("A$row", $no);
            $sheet->setCellValue("B$row", $value['Voltage']);
            $sheet->setCellValue("C$row", $value['Current']);
            $sheet->setCellValue("D$row", $value['Power']);
            $sheet->setCellValue("E$row", $value['Energy']);
            $sheet->setCellValue("F$row", $value['Frequency']);
            $sheet->setCellValue("G$row", $value['PowerFactor']);
            $sheet->setCellValue("H$row", $value['StatusValue']);
            $sheet->setCellValue("I$row", $value['created_at']);

            $row++;
            $no++;
        };

        $writer = new Xlsx($spreadsheet);
        // $writer->save($tabelName . '_' . time() . '.xlsx');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $tabelName . '_' . time() . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
        // echo 'berhasil';
    }
}
