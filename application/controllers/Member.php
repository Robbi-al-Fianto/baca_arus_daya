<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        // $data['getalldaya'] = $this->daya_arus->get_all_daya();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('member/index', $data);
        $this->load->view('templates/footer', $data);
    }
    public function sendmail()
    {
        $this->load->model('Gedung_m', 'gedung');
        $gedungfb = $this->gedung->getFB_gedung();
        if ($gedungfb['StatusValue'] == 1) {
            $html = '<button class="btn btn-success" type="submit">Device Connected</button>
            <br>';
            echo $html;
            if ($gedungfb['Current'] == 0) {
                $html =

                    '
                    
            <div class="row">
                <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Current</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Current'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Ampere (A)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            } else {
                $html =

                    '
        <div class="row">
            <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Current</div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Current'] . '</div></div>
                        <div class="col-auto">
                        <i class="fa-1x text-gray-300">Ampere (A)</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
                echo $html;
            }
            if ($gedungfb['Energy'] == 0) {
                $html =
                    '<div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1"> Energy</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Energy'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Watt-hour (Wh)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            } else {
                $html =
                    '<div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1"> Energy</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Energy'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Watt-hour (Wh)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            }


            if ($gedungfb['Frequency'] == 0) {
                $html =

                    '   <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1"> Frequency</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Frequency'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Hertz (Hz)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            } else {
                $html =

                    '   <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1"> Frequency</div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Frequency'] . '</div></div>
                        <div class="col-auto">
                        <i class="fa-1x text-gray-300">Hertz (Hz)</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
                echo $html;
            }
            if ($gedungfb['Power'] == 0) {
                $html =
                    '
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1"> Power</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Power'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Watt (W)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            } else {
                $html =
                    '
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1"> Power</div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Power'] . '</div></div>
                        <div class="col-auto">
                        <i class="fa-1x text-gray-300">Watt (W)</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
                echo $html;
            }
            if ($gedungfb['PowerFactor'] == 0) {
                $html =

                    '
    
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1"> Power Factor</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['PowerFactor'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Kilowatt-hour (kWh)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            } else {
                $html =  '
                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="card border-left-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1"> Power Factor</div>
                                    <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['PowerFactor'] . '</div></div>
                                <div class="col-auto">
                                <i class="fa-1x text-gray-300">Kilowatt-hour (kWh)</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
                echo $html;
            }
            if ($gedungfb['Voltage'] == 0) {
                $html =
                    '
    
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Voltage</div>
                                <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Voltage'] . '</div></div>
                            <div class="col-auto">
                            <i class="fa-1x text-gray-300">Volt (V)</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
                echo $html;
            } else {
                $html =
                    '

        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Voltage</div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Voltage'] . '</div></div>
                        <div class="col-auto">
                        <i class="fa-1x text-gray-300">Volt (V)</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
                echo $html;
            }

            '
            </div>
            <br>';
            // echo $html;
        } else {

            $html =

                '
                <button class="btn btn-danger" type="submit">Device Not Connected</button>
              

    <div class="row">
        <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Current</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' .  $gedungfb['Current'] . '</div></div>
                    <div class="col-auto">
                    <i class="fa-1x text-gray-300">Ampere (A)</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Energy</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Energy'] . '</div></div>
                    <div class="col-auto">
                    <i class="fa-1x text-gray-300">Watt-hour (Wh)</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Frequency</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Frequency'] . '</div></div>
                    <div class="col-auto">
                    <i class="fa-1x text-gray-300">Hertz (Hz)</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Power</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Power'] . '</div></div>
                    <div class="col-auto">
                    <i class="fa-1x text-gray-300">Watt (W)</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Power Factor</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['PowerFactor'] . '</div></div>
                    <div class="col-auto">
                    <i class="fa-1x text-gray-300">Kilowatt-hour (kWh)</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> Voltage</div>
                        <div class="h1 mb-0 font-weight-bold text-gray-800">' . $gedungfb['Voltage'] .  '</div></div>
                    <div class="col-auto">
                    <i class="fa-1x text-gray-300">Volt (V)</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    </div>
    <br>';
            echo $html;
        }
            // <div class="row">
            // <div class="col-sm-4 mb-4">
            // <div class="card text-dark bg-light">
            // <div class="card-body ">

            // <h5 class="card-title">From Firebase</h5>

            // <div class="list-group list-group-flush">
            // <a href="#" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-secondary disabled">Current <span class="badge badge-primary badge-pill">' . $gedungfb['Current'] . '</span></a>
            // <a href="#" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-secondary disabled">Energy <span class="badge badge-primary badge-pill">' . $gedungfb['Energy'] . '</span></a>
            // <a href="#" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-secondary disabled">Frequency <span class="badge badge-primary badge-pill">'  . $gedungfb['Frequency'] . '</span></a>
            // <a href="#" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-secondary disabled">Power <span class="badge badge-primary badge-pill">' . $gedungfb['Power'] .   '</span></a>
            // <a href="#" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-secondary disabled">Power Factor <span class="badge badge-primary badge-pill">' . $gedungfb['PowerFactor'] . '</span></a>
            // <a href="#" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-secondary disabled">Voltage <span class="badge badge-primary badge-pill">' . $gedungfb['Voltage'] .  '</span></a>
            // </div>

            // </div>
            // </div>
            // </div>
            // </div>



            // <li class="list-group-item">Energy       : ' . $gedungfb['Energy'] . '</li>
            // <li class="list-group-item">Frequency      : ' . $gedungfb['Frequency'] . '</li>
            // <li class="list-group-item">Power   : ' . $gedungfb['Power'] .  '</li>
            // <li class="list-group-item">PowerFactor : ' . $gedungfb['PowerFactor'] . '</li>
            // <li class="list-group-item">Voltage     : ' . $gedungfb['Voltage'] . '</li>
            // <li class="list-group-item">Updated_at  : ' . (isset($sdg['created_at'])  ? $sdg['created_at']  : ' ') .  '</li>




        ;


        // echo $html;
        // echo json_encode($gedungs);
    }
}
