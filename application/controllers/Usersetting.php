<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usersetting extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    public function index()
    {
        $data['title'] = 'User Setting Admin';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->model('Usersetting_model', 'usersetting');

        $data['User'] = $this->usersetting->getUser();

        $data['role'] = $this->db->get('user_role')->result_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('usersetting/index', $data);
        $this->load->view('templates/footer', $data);
    }

    function updateuser()
    {
        // $data['subMenu'] = $this->menu->getSubMenu();
        // $data['menu'] = $this->db->get('user_menu')->result_array();

        // $this->load->model('Menu_model', 'menu');

        $id       = $this->input->post('edit_id');
        $name    = $this->input->post('edit_name');
        $email      = $this->input->post('edit_email');
        $role     = $this->input->post('edit_role_id');
        $id       = trim($id);
        $name    = trim($name);
        $email     = trim($email);
        $role     = trim($role);

        $data = array(
            'name'     => $name,
            'email'       => $email,
            'role_id'   => $role
        );

        $this->db->where('id', $id);
        $this->db->update('user', $data);

        $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">
        User Updated</div>');

        redirect('Usersetting');
    }

    function deleteuser()
    {
        $id = $this->input->post('id');
        $this->db->delete('user', array('id' => $id));

        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        User Deleted</div>');

        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        User Deleted</div>');

        redirect('Usersetting');
    }
}
