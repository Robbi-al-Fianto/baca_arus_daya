

<?php

require "dbconnect.php";

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $response = array();

    $voltage = isset($_POST['Voltage']) ? $_POST['Voltage'] : null;
    $current = $_POST['Current'];
    $power = $_POST['Power'];
    $energy = $_POST['Energy'];
    $frequency = $_POST['Frequency'];
    $powerFactor = $_POST['PowerFactor'];
    $statusValue = $_POST['StatusValue'];


    $insert = "INSERT INTO sdg__1(Voltage, Current, Power, Energy, Frequency, PowerFactor, StatusValue) 
    VALUE('$voltage','$current','$power','$energy','$frequency','$powerFactor','$statusValue')";
    if (mysqli_query($koneksi, $insert)) {
        $response['value'] = 1;
        $response['message'] = "Data berhasil masuk";
        echo json_encode($response);
    } else {
        $response['value'] = 0;
        $response['message'] = "Data gagal masuk";
        echo json_encode($response);
    }
}
